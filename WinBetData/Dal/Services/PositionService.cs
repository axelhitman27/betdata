﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface IPositionService : IBaseService<Position>
    {
        IQueryable<Position> SelectAll();
        IQueryable<Position> SelectByCriteriaBase(CriteriaPosition criteria);
    }

    public class PositionService : BaseService<Position>, IPositionService
    {
        public IQueryable<Position> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Position> SelectByCriteriaBase(CriteriaPosition criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);

            return query;
        }
    }

    public class CriteriaPosition
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
