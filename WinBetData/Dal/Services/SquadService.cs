﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface ISquadService : IBaseService<Squad>
    {
        IQueryable<Squad> SelectAll();
        IQueryable<Squad> SelectByCriteriaBase(CriteriaSquad criteria);
    }

    public class SquadService : BaseService<Squad>, ISquadService
    {
        public IQueryable<Squad> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Squad> SelectByCriteriaBase(CriteriaSquad criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (criteria.Number > 0)
                query = query.Where(x => x.Number == criteria.Number);
            if (criteria.PositionId > 0)
                query = query.Where(x => x.PositionId == criteria.PositionId);
            if (criteria.TeamId > 0)
                query = query.Where(x => x.TeamId == criteria.TeamId);
            if (criteria.PlayerId > 0)
                query = query.Where(x => x.PlayerId == criteria.PlayerId);

            return query;
        }
    }

    public class CriteriaSquad
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int PositionId { get; set; }
        public int TeamId { get; set; }
        public int PlayerId { get; set; }
    }
}
