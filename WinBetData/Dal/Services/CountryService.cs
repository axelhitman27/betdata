﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Helpers;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface ICountryService : IBaseService<Country>
    {
        IQueryable<Country> SelectAll();
        IQueryable<Country> SelectByCriteriaBase(CriteriaCountry criteria);
    }

    public class CountryService : BaseService<Country>, ICountryService
    {
        public IQueryable<Country> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Country> SelectByCriteriaBase(CriteriaCountry criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);
            if (!criteria.SpecialName.IsNullOrEmpty())
                query = query.Where(x => x.SpecialName == criteria.SpecialName);

            return query;
        }
    }

    public class CriteriaCountry
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SpecialName { get; set; }
    }
}
