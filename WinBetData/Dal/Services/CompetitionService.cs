﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Helpers;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface ICompetitionService : IBaseService<Competition>
    {
        IQueryable<Competition> SelectAll();
        IQueryable<Competition> SelectByCriteriaBase(CriteriaCompetition criteria);
    }

    public class CompetitionService : BaseService<Competition>, ICompetitionService
    {
        public IQueryable<Competition> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Competition> SelectByCriteriaBase(CriteriaCompetition criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);
            if (criteria.LeagueId > 0)
                query = query.Where(x => x.LeagueId == criteria.LeagueId);
            if (criteria.SeasonId > 0)
                query = query.Where(x => x.SeasonId == criteria.SeasonId);

            query = query.Where(x => criteria.StartAt <= x.StartAt && criteria.EndAt >= x.EndAt);

            return query;
        }
    }

    public class CriteriaCompetition
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LeagueId { get; set; }
        public int SeasonId { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
    }
}
