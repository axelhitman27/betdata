﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface ITeamService : IBaseService<Team>
    {
        IQueryable<Team> SelectAll();
        IQueryable<Team> SelectByCriteriaBase(CriteriaTeam criteria);
    }

    public class TeamService : BaseService<Team>, ITeamService
    {
        public IQueryable<Team> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Team> SelectByCriteriaBase(CriteriaTeam criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);
            if (!criteria.SpecialName.IsNullOrEmpty())
                query = query.Where(x => x.SpecialName == criteria.SpecialName);
            if (criteria.CompetitionId > 0)
                query = query.Where(x => x.CompetitionId == criteria.CompetitionId);

            return query;
        }
    }

    public class CriteriaTeam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SpecialName { get; set; }
        public int CompetitionId { get; set; }
    }
}
