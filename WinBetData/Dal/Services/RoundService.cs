﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface IRoundService : IBaseService<Round>
    {
        IQueryable<Round> SelectAll();
        IQueryable<Round> SelectByCriteriaBase(CriteriaRound criteria);
    }

    public class RoundService : BaseService<Round>, IRoundService
    {
        public IQueryable<Round> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Round> SelectByCriteriaBase(CriteriaRound criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);

            query = query.Where(x => x.StartAt >= criteria.StartAt);

            query = query.Where(x => x.EndAt <= criteria.EndAt);

            if(criteria.Number > 0)
                query = query.Where(x => x.Number == criteria.Number);
            if(criteria.CompetitionId > 0)
                query = query.Where(x => x.CompetitionId == criteria.CompetitionId);

            return query;
        }
    }

    public class CriteriaRound
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public int Number { get; set; }
        public int CompetitionId { get; set; }
    }
}
