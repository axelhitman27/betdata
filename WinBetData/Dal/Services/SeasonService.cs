﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface ISeasonService : IBaseService<Season>
    {
        IQueryable<Season> SelectAll();
        IQueryable<Season> SelectByCriteriaBase(CriteriaSeason criteria);
    }

    public class SeasonService : BaseService<Season>, ISeasonService
    {
        public IQueryable<Season> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Season> SelectByCriteriaBase(CriteriaSeason criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);

            return query;
        }
    }

    public class CriteriaSeason
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
