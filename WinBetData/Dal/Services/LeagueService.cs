﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface ILeagueService : IBaseService<League>
    {
        IQueryable<League> SelectAll();
        IQueryable<League> SelectByCriteriaBase(CriteriaLeague criteria);
    }

    public class LeagueService : BaseService<League>, ILeagueService
    {
        public IQueryable<League> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<League> SelectByCriteriaBase(CriteriaLeague criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);

            return query;
        }
    }

    public class CriteriaLeague
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
