﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface IGoalService : IBaseService<Goal>
    {
        IQueryable<Goal> SelectAll();
        IQueryable<Goal> SelectByCriteriaBase(CriteriaGoal criteria);
    }

    public class CountryGoal : BaseService<Goal>, IGoalService
    {
        public IQueryable<Goal> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Goal> SelectByCriteriaBase(CriteriaGoal criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (criteria.Minute > 0)
                query = query.Where(x => x.Minute == criteria.Minute);
            if (criteria.PlayerId > 0)
                query = query.Where(x => x.MatchId == criteria.MatchId);

            query = query.Where(x => x.Penalty == criteria.Penalty);

            query = query.Where(x => x.OwnGoal == criteria.OwnGoal);

            return query;
        }
    }

    public class CriteriaGoal
    {
        public int Id { get; set; }
        public int Minute { get; set; }
        public bool Penalty { get; set; }
        public bool OwnGoal { get; set; }
        public int PlayerId { get; set; }
        public int MatchId { get; set; }
    }
}
