﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Helpers;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface IGroupService : IBaseService<Group>
    {
        IQueryable<Group> SelectAll();
        IQueryable<Group> SelectByCriteriaBase(CriteriaGroup criteria);
    }

    public class GroupService : BaseService<Group>, IGroupService
    {
        public IQueryable<Group> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Group> SelectByCriteriaBase(CriteriaGroup criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);
            if (criteria.CompetitionId > 0)
                query = query.Where(x => x.CompetitionId == criteria.CompetitionId);

            return query;
        }
    }

    public class CriteriaGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompetitionId { get; set; }
    }
}
