﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Services._Common;
using Dal.Helpers;

namespace Dal.Services
{
    public interface IPlayerService : IBaseService<Player>
    {
        IQueryable<Player> SelectAll();
        IQueryable<Player> SelectByCriteriaBase(CriteriaPlayer criteria);
    }

    public class PlayerService : BaseService<Player>, IPlayerService
    {
        public IQueryable<Player> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Player> SelectByCriteriaBase(CriteriaPlayer criteria)
        {
            var query = Query();
            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);
            if (!criteria.Name.IsNullOrEmpty())
                query = query.Where(x => x.Name == criteria.Name);

            return query;
        }
    }

    public class CriteriaPlayer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
