﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Entities;
using Dal.Helpers;
using Dal.Services._Common;

namespace Dal.Services
{
    public interface IMatchService : IBaseService<Match>
    {
        IQueryable<Match> SelectAll();
        IQueryable<Match> SelectByCriteriaBase(CriteriaMatch criteria);
    }

    public class MatchService : BaseService<Match>, IMatchService
    {
        public IQueryable<Match> SelectAll()
        {
            var query = Query();

            return query;
        }
        public IQueryable<Match> SelectByCriteriaBase(CriteriaMatch criteria)
        {
            var query = Query();

            if (criteria.Id > 0)
                query = query.Where(x => x.Id == criteria.Id);

            query = query.Where(x => x.Date == criteria.Date);

            if (criteria.OddHome > 0)
                query = query.Where(x => x.OddHome == criteria.OddHome);
            if (criteria.HomeId > 0)
                query = query.Where(x => x.HomeId == criteria.HomeId);
            if (criteria.OddDraw > 0)
                query = query.Where(x => x.OddDraw >= criteria.OddDraw);
            if(criteria.AwayId > 0)
                query = query.Where(x => x.AwayId == criteria.AwayId);
            if (criteria.OddAway > 0)
                query = query.Where(x => x.OddAway >= criteria.OddAway);
            if (criteria.OddOver > 0)
                query = query.Where(x => x.OddOver >= criteria.OddOver);
            if (criteria.OddUnder > 0)
                query = query.Where(x => x.OddUnder >= criteria.OddUnder);
            if (criteria.OddGoalGoal > 0)
                query = query.Where(x => x.OddGoalGoal >= criteria.OddGoalGoal);
            if (criteria.OddNoGoal > 0)
                query = query.Where(x => x.OddNoGoal >= criteria.OddNoGoal);

            query = query.Where(x => x.Won == criteria.Won);

            if(!criteria.PlayAt.IsNullOrEmpty())
                query = query.Where(x => x.PlayAt == criteria.PlayAt);

            query = query.Where(x => x.Knockout == criteria.Knockout);

            if (criteria.WinnerId > 0)
                query = query.Where(x => x.WinnerId == criteria.WinnerId);

            if (criteria.ScoreHome >= 0)
                query = query.Where(x => x.ScoreHome == criteria.ScoreHome);
            if (criteria.ScoreAway >= 0)
                query = query.Where(x => x.ScoreAway == criteria.ScoreAway);
            if (criteria.ScoreExtraHome >= 0)
                query = query.Where(x => x.ScoreExtraHome == criteria.ScoreExtraHome);
            if (criteria.ScoreExtraAway >= 0)
                query = query.Where(x => x.ScoreExtraAway == criteria.ScoreExtraAway);
            if (criteria.ScorePenaltyHome >= 0)
                query = query.Where(x => x.ScorePenaltyHome == criteria.ScorePenaltyHome);
            if (criteria.ScorePenaltyAway >= 0)
                query = query.Where(x => x.ScorePenaltyAway == criteria.ScorePenaltyAway);
            if (criteria.GroupId > 0)
                query = query.Where(x => x.GroupId == criteria.GroupId);
            if (criteria.RoundId > 0)
                query = query.Where(x => x.RoundId == criteria.RoundId);

            return query;
        }
    }

    public class CriteriaMatch
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal OddHome { get; set; }
        public int HomeId { get; set; }
        public decimal OddDraw { get; set; }
        public int AwayId { get; set; }
        public decimal OddAway { get; set; }
        public decimal OddOver { get; set; }
        public decimal OddUnder { get; set; }
        public decimal OddGoalGoal { get; set; }
        public decimal OddNoGoal { get; set; }
        public bool Won { get; set; }
        public string PlayAt { get; set; }
        public bool Knockout { get; set; }
        public int WinnerId { get; set; }
        public int ScoreHome { get; set; }
        public int ScoreAway { get; set; }
        public int ScoreExtraHome { get; set; }
        public int ScoreExtraAway { get; set; }
        public int ScorePenaltyHome { get; set; }
        public int ScorePenaltyAway { get; set; }
        public int GroupId { get; set; }
        public int RoundId { get; set; }

    }
}
