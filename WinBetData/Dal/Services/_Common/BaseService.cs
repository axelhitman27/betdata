﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Helpers;

namespace Dal.Services._Common
{
    public interface IBaseService<T> : IDisposable where T : class, new()
    {
        IQueryable<T> Query(BetDataContext context = null);
        T SaveOrUpdate(T entity, string keyField = "Id", BetDataContext context = null);
        T Add(T entity, BetDataContext context = null);
    }
    public abstract class BaseService<T> : IBaseService<T> where T : class, new()
    {
        private BetDataContext _context;

        public BetDataContext GetDataContext(bool reuse = false)
        {
            if (reuse && _context != null) return _context;

            _context = new BetDataContext();

            return _context;
        }
        public virtual T SaveOrUpdate(T entity, string keyField = "Id", BetDataContext context = null)
        {
            var id = GetFieldValue(entity, keyField).MyExtToInt();
            if (id == 0)
                return Add(entity, context);
            return Update(entity, context);
        }

        public virtual T Update(T entity, BetDataContext context1 = null)
        {
            if (context1 != null)
            {
                var entry = context1.Entry(entity);
                entry.State = EntityState.Modified;
                context1.SaveChanges();
                return entity;
            }

            Dispose();
            using (var context = GetDataContext())
            {
                var entry = context.Entry(entity);
                entry.State = EntityState.Modified;
                context.SaveChanges();
            }
            return entity;
        }

        public void Dispose()
        {
            DisposeContext();
        }

        public void DisposeContext()
        {
            if (_context != null)
            {
                _context.Dispose();
                _context = null;
            }
        }

        public object GetFieldValue(T entity, string keyField = "Id")
        {
            return entity.GetType().GetProperty(keyField)?.GetValue(entity, null);
        }

        public virtual IQueryable<T> Query(BetDataContext context = null)
        {
            var dbSet = (context ?? GetDataContext()).Set<T>();
            return dbSet.AsNoTracking();
        }

        public virtual T Add(T entity, BetDataContext context1 = null)
        {
            if (context1 != null)
            {
                var dbSet = context1.Set<T>();

                dbSet.Add(entity);
                context1.SaveChanges();
                return entity;
            }

            using (var context = GetDataContext())
            {
                var dbSet = context.Set<T>();
                dbSet.Add(entity);
                context.SaveChanges();
            }
            return entity;
        }
    }
}
