﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Dal.Entities;

namespace Dal
{
    public class BetDataContext:DbContext
    {
        public BetDataContext(string connString = null, int timeout = 160)
        {
            Database.Connection.ConnectionString = connString ?? Db.Home;
            var objectContextAdapter = this as IObjectContextAdapter;
            var objectContext = objectContextAdapter.ObjectContext;
            objectContext.CommandTimeout = timeout;
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Competition> Competitions { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Round> Rounds { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Team> Teams { get; set; }
    }
}
