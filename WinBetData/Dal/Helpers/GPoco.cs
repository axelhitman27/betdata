﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Helpers
{
    public static class GPoco
    {
        public const string NumberFormat = "#,#.##";
        public static Random Random = new Random();
        public static int ModeOpen;
        public static CultureInfo Cinfen = new CultureInfo("en-US");
        public static object Missing = System.Reflection.Missing.Value;

        public static DateTime ZeroDate => DateTime.Parse("1899-01-01 00:00:00");

        public static string Eu => "EU";
    }
}
