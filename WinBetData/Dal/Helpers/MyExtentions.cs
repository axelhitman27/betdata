﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Helpers
{
    public static class MyExtentions
    {
        public const string NumberFormat = "#,#.##";
        public static CultureInfo Cinfru = new CultureInfo("ru-RU");
        public static CultureInfo Cinfen = new CultureInfo("en-US");

        public static int MyExtToInt(this object tt)
        {
            if (tt == null) return 0;

            if (tt is bool)
                return (bool)tt ? 1 : 0;

            var ttemp = tt.ToString();

            if (ttemp == "" || ttemp == " " || ttemp.Trim() == "0")
                return 0;

            ttemp = ttemp.Replace(",", ".");
            if (ttemp.Contains(".")) ttemp = ttemp.Substring(0, ttemp.IndexOf(".", StringComparison.Ordinal));
            int tmpout;
            var dbl = int.TryParse(ttemp, NumberStyles.Float, GPoco.Cinfen, out tmpout);
            if (dbl) return tmpout;

            return 0;
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
    }
}
