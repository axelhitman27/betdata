﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("League")]
    public class League
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
