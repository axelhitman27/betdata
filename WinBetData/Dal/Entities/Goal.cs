﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("Goal")]
    public class Goal
    {
        public int Id { get; set; }
        public int Minute { get; set; }
        public bool Penalty { get; set; }
        public bool OwnGoal { get; set; }
        public int PlayerId { get; set; }
        public int MatchId { get; set; }
        [ForeignKey("PlayerId")]
        public virtual Player Players { get; set; }
        [ForeignKey("MatchId")]
        public virtual Match Matches { get; set; }
    }
}
