﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    [Table("Team")]
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SpecialName { get; set; }
        public int CompetitionId { get; set; }
        [ForeignKey("CompetitionId")]
        public virtual Competition Competition { get; set; }
    }
}
