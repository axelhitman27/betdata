﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dal.Entities
{
    [Table("Match")]
    public class Match
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal OddHome { get; set; }
        public int HomeId { get; set; }
        public decimal OddDraw { get; set; }
        public int AwayId { get; set; }
        public decimal OddAway { get; set; }
        public decimal OddOver { get; set; }
        public decimal OddUnder { get; set; }
        public decimal OddGoalGoal { get; set; }
        public decimal OddNoGoal { get; set; }
        public bool Won { get; set; }
        public string PlayAt { get; set; }
        public bool Knockout { get; set; }
        public int WinnerId { get; set; }
        public int ScoreHome { get; set; }
        public int ScoreAway { get; set; }
        public int ScoreExtraHome { get; set; }
        public int ScoreExtraAway{ get; set; }
        public int ScorePenaltyHome { get; set; }
        public int ScorePenaltyAway { get; set; }
        public int GroupId { get; set; }
        public int RoundId { get; set; }
        [ForeignKey("HomeId")]
        public Team TeamHome { get; set; }
        [ForeignKey("AwayId")]
        public Team TeamAway { get; set; }
        [ForeignKey("WinnerId")]
        public virtual Team TeamWinner { get; set; }
        [ForeignKey("GroupId")]
        public virtual Group Groups { get; set; }
        [ForeignKey("RoundId")]
        public virtual Round Rounds { get; set; }
    }
}
