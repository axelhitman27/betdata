﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("Season")]
    public class Season
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
