﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("Squad")]
    public class Squad
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public int PositionId { get; set; }
        public int TeamId { get; set; }
        public int PlayerId { get; set; }
        [ForeignKey("PositionId")]
        public virtual Position Position { get; set; }
        [ForeignKey("TeamId")]
        public virtual Team Teams { get; set; }
        [ForeignKey("PlayerId")]
        public virtual Player Players { get; set; }
    }
}
