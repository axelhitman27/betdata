﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("Group")]
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompetitionId { get; set; }
        [ForeignKey("CompetitionId")]
        public virtual Competition Competitions { get; set; }
    }
}
