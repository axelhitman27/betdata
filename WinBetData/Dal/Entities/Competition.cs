﻿using Dal.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    [Table("Competition")]
    public class Competition
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LeagueId { get; set; }
        public int SeasonId { get; set; }

        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }

        [ForeignKey("LeagueId")]
        public virtual League Leagues { get; set; }
        [ForeignKey("SeasonId")]
        public virtual Season Seasons { get; set; }
    }
}
