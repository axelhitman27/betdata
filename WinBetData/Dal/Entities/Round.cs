﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Entities
{
    [Table("Round")]
    public class Round
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public int Number { get; set; }
        public int CompetitionId { get; set; }
        [ForeignKey("CompetitionId")]
        public virtual Competition Competitions { get; set; }
    }
}
