﻿namespace WinBetData.frms
{
    partial class frmTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdTeams = new DevExpress.XtraGrid.GridControl();
            this.grdvTeams = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.grdTeams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdvTeams)).BeginInit();
            this.SuspendLayout();
            // 
            // grdTeams
            // 
            this.grdTeams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdTeams.Location = new System.Drawing.Point(12, 12);
            this.grdTeams.MainView = this.grdvTeams;
            this.grdTeams.Name = "grdTeams";
            this.grdTeams.Size = new System.Drawing.Size(958, 527);
            this.grdTeams.TabIndex = 0;
            this.grdTeams.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdvTeams});
            // 
            // grdvTeams
            // 
            this.grdvTeams.GridControl = this.grdTeams;
            this.grdvTeams.Name = "grdvTeams";
            // 
            // frmTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 551);
            this.Controls.Add(this.grdTeams);
            this.Name = "frmTeam";
            this.Text = "Teams";
            ((System.ComponentModel.ISupportInitialize)(this.grdTeams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdvTeams)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdTeams;
        private DevExpress.XtraGrid.Views.Grid.GridView grdvTeams;
    }
}