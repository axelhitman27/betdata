﻿namespace WinBetData.frms
{
    partial class frmCountry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdCountry = new DevExpress.XtraGrid.GridControl();
            this.grdvCountry = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialName = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdvCountry)).BeginInit();
            this.SuspendLayout();
            // 
            // grdCountry
            // 
            this.grdCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCountry.Location = new System.Drawing.Point(0, 0);
            this.grdCountry.MainView = this.grdvCountry;
            this.grdCountry.Name = "grdCountry";
            this.grdCountry.Size = new System.Drawing.Size(1227, 612);
            this.grdCountry.TabIndex = 0;
            this.grdCountry.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grdvCountry});
            // 
            // grdvCountry
            // 
            this.grdvCountry.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName,
            this.colSpecialName});
            this.grdvCountry.GridControl = this.grdCountry;
            this.grdvCountry.Name = "grdvCountry";
            this.grdvCountry.OptionsView.EnableAppearanceOddRow = true;
            this.grdvCountry.OptionsView.ShowAutoFilterRow = true;
            this.grdvCountry.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colId.AppearanceHeader.Options.UseFont = true;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.Caption = "Χώρα";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colSpecialName
            // 
            this.colSpecialName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.colSpecialName.AppearanceHeader.Options.UseFont = true;
            this.colSpecialName.Caption = "Ακρωνύμιο";
            this.colSpecialName.FieldName = "SpecialName";
            this.colSpecialName.Name = "colSpecialName";
            this.colSpecialName.Visible = true;
            this.colSpecialName.VisibleIndex = 1;
            // 
            // frmCountry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 612);
            this.Controls.Add(this.grdCountry);
            this.Name = "frmCountry";
            this.Text = "Countries";
            ((System.ComponentModel.ISupportInitialize)(this.grdCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdvCountry)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl grdCountry;
        private DevExpress.XtraGrid.Views.Grid.GridView grdvCountry;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialName;
    }
}